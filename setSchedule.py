import json
import os
import sys

timeschedule_template = {
  "Mon": {
    "1": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "2": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "3": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "4": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "5": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "6": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    }
  },
  "Tue": {
    "1": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "2": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "3": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "4": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "5": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "6": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    }
  },
  "Wed": {
    "1": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "2": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "3": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "4": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "5": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "6": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    }
  },
  "Thu": {
    "1": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "2": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "3": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "4": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "5": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "6": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    }
  },
  "Fri": {
    "1": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "2": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "3": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "4": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "5": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "6": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    }
  },
  "Sat": {
    "1": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "2": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "3": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "4": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "5": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    },
    "6": {
      "class_name": "",
      "teacher_name": "",
      "classroom": "",
      "zoom_id": "",
      "zoom_pass": "",
      "links": {
        "link1": {
          "name": "",
          "link": ""
        },
        "link2": {
          "name": "",
          "link": ""
        }
      }
    }
  }
}

def get_dir(filename):
  if getattr(sys, 'frozen', False):
    current_dir = os.path.dirname(sys._MEIPASS)+"\\_internal"
  else:
    current_dir = os.path.dirname(os.path.abspath(__file__))
  return os.path.join(current_dir, filename)

def set_timeschedule_template():
  filename = get_dir('settings\\timeschedule.json')
  with open(filename, 'w+', encoding='utf-8') as f:
    json.dump(timeschedule_template, f, ensure_ascii=False, indent=2)