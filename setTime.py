import json
import os
import sys

time_template = {
  "time": [
    [
      "",
      ""
    ],
    [
      "",
      ""
    ],
    [
      "",
      ""
    ],
    [
      "",
      ""
    ],
    [
      "",
      ""
    ],
    [
      "",
      ""
    ]
  ]
}
def get_dir(filename):
  if getattr(sys, 'frozen', False):
    current_dir = os.path.dirname(sys._MEIPASS)+"\\_internal"
  else:
    current_dir = os.path.dirname(os.path.abspath(__file__))
  return os.path.join(current_dir, filename)


def set_time_template():
  filename = get_dir('settings\\time.json')
  with open(filename, 'w+', encoding='utf-8') as f:
    json.dump(time_template, f, ensure_ascii=False, indent=2)
