import sys, io
sys.stdout = io.StringIO()
sys.stderr = io.StringIO()
import eel
import json
import os
import setSchedule as ss
import setTime as st

# exe化したとき用 ファイルパスを取得
def get_dir(filename):
  if getattr(sys, 'frozen', False):
    current_dir = os.path.dirname(sys._MEIPASS) + '\\_internal'
  else:
    current_dir = os.path.dirname(os.path.abspath(__file__))
  return os.path.join(current_dir, filename)

@eel.expose
def get_start_end_time():
  filename = get_dir('settings\\time.json')
  if not os.path.exists(filename):
    st.set_time_template()
  with open(filename, 'r', encoding='utf-8') as f:
    start_end_time = json.load(f)
  return start_end_time

@eel.expose
def set_start_end_time(start_end_time):
  filename = get_dir('settings\\time.json')
  with open(filename, 'w+', encoding='utf-8') as f:
    json.dump(start_end_time, f, ensure_ascii=False, indent=2)

@eel.expose
def get_timeschedule():
  filename = get_dir('settings\\timeschedule.json')
  if not os.path.exists(filename):
    ss.set_timeschedule_template()
  with open(filename, 'r', encoding='utf-8') as f:
    timeschedule = json.load(f)
  return timeschedule
@eel.expose
def set_timeschedule(timeschedule):
  filename = get_dir('settings\\timeschedule.json')
  with open(filename, 'w+', encoding='utf-8') as f:
    json.dump(timeschedule, f, ensure_ascii=False, indent=2)

@eel.expose
def new_window(target: str):
  eel.show(target)




eel.init('web')
eel.start('index.html', size=(950,600), port=0, mode='chrome')
