# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['ClassLink.py'],
    pathex=[],
    binaries=[],
    datas=[('.\\web\\index.html', '.\\web'),
    ('icon.ico', '.\\web'),
    ('.\\web\\css\\destyle.css', '.\\web\\css'),
    ('.\\web\\css\\style.css', '.\\web\\css'),
    ('.\\web\\fonts\\DSEG7Classic-Bold.ttf', '.\\web\\fonts'),
    ('.\\web\\img\\copy_btn.png', '.\\web\\img'),
    ('.\\web\\img\\eye_slash.png', '.\\web\\img'),
    ('.\\web\\img\\eye.png', '.\\web\\img'),
    ('.\\web\\img\\popup.png', '.\\web\\img'),
    ('.\\web\\img\\setting_button.png', '.\\web\\img'),
    ('.\\web\\img\\thumb_up.png', '.\\web\\img'),
    ('.\\web\\img\\x_white.png', '.\\web\\img'),
    ('.\\web\\js\\basicFnc.js', '.\\web\\js'),
    ('.\\web\\js\\fnc.js', '.\\web\\js'),
    ('.\\web\\js\\main.js', '.\\web\\js'),
    ('.\\web\\js\\popup.js', '.\\web\\js'),
    ('.\\settings\\time.json', '.\\settings'),
    ('.\\settings\\timeschedule.json', '.\\settings')
    ],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='ClassLink',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=['icon2.ico'],
)
coll = COLLECT(
    exe,
    a.binaries,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='ClassLink',
)
