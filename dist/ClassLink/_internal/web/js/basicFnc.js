/////////////////////////////////////////////////////////////
// 曜日,時間を取得
/////////////////////////////////////////////////////////////
function getCurrentTime() {
  var now = new Date
  var hour = now.getHours();
  var minute = now.getMinutes();
  var second = now.getSeconds();
  return [hour, minute, second];
}

function getCurrentDate() {
  var now = new Date
  var year = now.getFullYear();
  var month = now.getMonth();
  var date = now.getDate();
  var dayOfWeek = now.getDay();
  return [year, month, date, dayOfWeek];
}


/////////////////////////////////////////////////////////////
// 画面サイズ
/////////////////////////////////////////////////////////////
function getCurrentWindowSize() {
  return [window.innerWidth, document.documentElement.clientHeight];
}

function resizeWindow() {
  const [windowWidth, windowHeight] = getCurrentWindowSize();
  document.documentElement.style.setProperty("--window-width", windowWidth);
  document.documentElement.style.setProperty("--window-height", windowHeight);
  body = document.getElementsByTagName('body')[0];
  body.style.width = windowWidth + 'px';
  body.style.height = windowHeight + 'px';
}

/////////////////////////////////////////////////////////////
// クラスの追加削除
/////////////////////////////////////////////////////////////
function removeClass(element, className) {
  if (element.classList.contains(className)) {
    element.classList.remove(className);
  }
}
function addClass(element, className) {
  element.classList.add(className);
}


/////////////////////////////////////////////////////////////
// 文字列成形
/////////////////////////////////////////////////////////////
function zeroPad(num) {
  return num < 10 ? '0' + num : String(num);
}


/////////////////////////////////////////////////////////////
// 新しいウィンドウを開く
/////////////////////////////////////////////////////////////
function openNewWindow(url) {
  eel.new_window(url);
}

