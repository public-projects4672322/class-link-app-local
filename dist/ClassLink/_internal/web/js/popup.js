/////////////////////////////////////////////////////////////
// 授業クリック時のポップ画面
/////////////////////////////////////////////////////////////
// コマをクリックしたら、ポップアップさせる
async function popupClickedTd(id) {
  var popup = document.getElementById("popup_wrapper");
  var popupBg = document.getElementById("popup_bg");

  // ポップアップの表示
  removeClass(popup, "invisible");
  removeClass(popupBg, "invisible");

  // ポップアップのエレメントを取得
  var popupElements = await getElementOfPopup();
  
  // ポップアップの内容 - タイトル
  var dayOfWeek = ["月", "火", "水", "木", "金", "土"];
  var id = id.split(',');
  var day = id[0]-1; // 曜日
  var classTime = id[1]; // 時限
  // 内容表示
  popupElements[0].innerHTML = dayOfWeek[day] + '曜日' + classTime + '時限目'; // 曜日と時限を表示
  popupElements[1].value = timescheduleByDay[day][classTime].class_name;
  popupElements[2].value = timescheduleByDay[day][classTime].classroom;
  popupElements[3].value = timescheduleByDay[day][classTime].teacher_name;
  popupElements[4].value = timescheduleByDay[day][classTime].zoom_id;
  popupElements[5].value = timescheduleByDay[day][classTime].zoom_pass;
  popupElements[6].value = timescheduleByDay[day][classTime].links.link1.name;
  popupElements[7].value = timescheduleByDay[day][classTime].links.link1.link;
  popupElements[8].value = timescheduleByDay[day][classTime].links.link2.name;
  popupElements[9].value = timescheduleByDay[day][classTime].links.link2.link;
  // ポップアップ起動時の設定
  togglePasswordDisplay(1);
}
// ポップ画面を閉じる
function closePopup() {
  var popups = document.getElementsByClassName("popup_wrapper");
  var popupBg = document.getElementById("popup_bg");
  for (var popup of popups) {
    addClass(popup, "invisible");
  }
  addClass(popupBg, "invisible");
}
// パスワード表示非表示
function togglePasswordDisplay(mode = 0) {
  var element = document.getElementById("popup_password_input");
  var eye = document.getElementById("popup_password_td_eye_img");
  if (mode == 1) {
    element.type = "password";
    eye.src = "./img/eye_slash.png";
    return;
  }
  if (element.type == "password") {
    element.type = "text";
    eye.src = "./img/eye.png";
  } else {
    element.type = "password";
    eye.src = "./img/eye_slash.png";
  }
}
// リンクをクリックしたら開く
function popupLink(id) {
  var element = document.getElementById(id);
  var link = element.value;
  if (link) {
    window.open(link);
  }
}
// ポップアップに書き込まれた内容を保存
async function saveContentOfPopup() {
  // ポップアップのエレメントを取得
  var popupElements = await getElementOfPopup();
  var popupElementsValues = [];
  for (var i = 1; i < popupElements.length; i++){
    popupElementsValues[i] = popupElements[i].value;
  }
  // 曜日、時限を取得
  var day = popupElements[0].innerHTML.slice(0, 1);
  var classtime = popupElements[0].innerHTML.slice(3, 4);
  var dayToNum = {"月": 0, "火": 1, "水": 2, "木": 3, "金": 4, "土": 5};
  await setPopupContentTotimescheduleByDayTime(timescheduleByDay[dayToNum[day]][classtime], popupElementsValues);

  // Jsonファイルに書き出す
  setTimeschedule();
}
// 内容を削除する
async function deleteContentOfPopup() {
  // 本当に削除するか確認する
  var confirmation = await deleteComfirmation();
  if (confirmation == false) { return; }
  var classDayTime = document.getElementById("popup_class_day_time");
  // 曜日、時限を取得
  var day = classDayTime.innerHTML.slice(0, 1);
  var classtime = classDayTime.innerHTML.slice(3, 4);
  var dayToNum = {"月": 0, "火": 1, "水": 2, "木": 3, "金": 4, "土": 5};
  await setPopupContentTotimescheduleByDayTime(timescheduleByDay[dayToNum[day]][classtime], []);
  setTimeschedule();
}


function getElementOfPopup() {
  // ポップアップのエレメントを取得
  var classDayTime = document.getElementById("popup_class_day_time");
  var className = document.getElementById("popup_class_name_input");
  var classroom = document.getElementById("popup_classroom_input");
  var teacher = document.getElementById("popup_teacher_input");
  var zoomId = document.getElementById("popup_zoom_id_input");
  var password = document.getElementById("popup_password_input");
  var link1Title = document.getElementById("popup_link1_title");
  var link1 = document.getElementById("popup_link1_input");
  var link2Title = document.getElementById("popup_link2_title");
  var link2 = document.getElementById("popup_link2_input");
  return new Promise((resolve) => {
    resolve([classDayTime, className, classroom, teacher, zoomId, password, link1Title, link1, link2Title, link2]);
  })
}
function setPopupContentTotimescheduleByDayTime(timescheduleByDayTime, popupElementsValues = []) {
  if (!popupElementsValues) {
    popupElementsValues = new Array(9);
  }
  timescheduleByDayTime.class_name = popupElementsValues[1];
  timescheduleByDayTime.classroom = popupElementsValues[2];
  timescheduleByDayTime.teacher_name = popupElementsValues[3];
  timescheduleByDayTime.zoom_id = popupElementsValues[4];
  timescheduleByDayTime.zoom_pass = popupElementsValues[5];
  timescheduleByDayTime.links.link1.name = popupElementsValues[6];
  timescheduleByDayTime.links.link1.link = popupElementsValues[7];
  timescheduleByDayTime.links.link2.name = popupElementsValues[8];
  timescheduleByDayTime.links.link2.link = popupElementsValues[9];
  return new Promise((resolve) => {
    resolve();
  })
}
// Jsonファイルに書き出す関数
async function setTimeschedule() {
  await eel.set_timeschedule(timeschedule);
  window.location.reload();
}
// 削除確認画面
function deleteComfirmation() {
  var confirmation = window.confirm("本当に削除しますか?\r\n一度削除したものは復元できません。");
  return new Promise((resolve) => {
    resolve(confirmation);
  })
}










/////////////////////////////////////////////////////////////
// 設定画面のポップアップ
/////////////////////////////////////////////////////////////
function popupSettingWindow() {
  var popup = document.getElementById("setting_wrapper");
  var popupBg = document.getElementById("popup_bg");
  // ポップアップの表示
  removeClass(popup, "invisible");
  removeClass(popupBg, "invisible");
  // 最初の画面を出す
  popupSettingContentSetTimeOfClass();
}

/////////////////////////////////////////////////////////////
// 時間設定画面
function popupSettingContentSetTimeOfClass() {
  var wrapper = document.getElementById("popup_setting_selected_content");
  // 時間の自動入力機能画面
  var autoInput = document.createElement("div");
  autoInput.setAttribute("class", "popup_setting_auto_input");
  var title = document.createElement("h4");
  title.innerHTML = "時間の自動入力";
  autoInput.appendChild(title);
  var autoInputTexts = ["1限開始時刻", "授業時間","授業の間隔", "昼休みの長さ"];
  // 初期値代入
  var autoInputValues = ["09:00", "100","10", "50"];
  for (var i = 0; i < 4; i++) {
    var row = document.createElement("div");
    var title = document.createElement("h5");
    title.innerHTML = autoInputTexts[i];
    var input = document.createElement("input");
    input.setAttribute("id", "popup_setting_auto_input" + i);
    if (i == 0) { input.setAttribute("type", "time"); }
    else { input.setAttribute("type", "number"); }
    input.value = autoInputValues[i];
    row.appendChild(title);
    row.appendChild(input);
    autoInput.appendChild(row);
  }
  // 自動入力ボタン
  var autoInputButtonWrapper = document.createElement("div");
  var autoInputButton = document.createElement("button");
  autoInputButton.setAttribute("id", "popup_setting_auto_input_button");
  autoInputButton.innerHTML = "自動入力";
  autoInputButton.setAttribute("onclick", "settingTimeAutoInput()");
  autoInputButtonWrapper.appendChild(autoInputButton);
  autoInput.appendChild(autoInputButtonWrapper);
  wrapper.appendChild(autoInput);
  
  // 手入力用
  var manualInput = document.createElement("div");
  manualInput.setAttribute("class", "popup_setting_manual_input");
  var manualInputTitle = document.createElement("h4");
  manualInputTitle.innerHTML = "授業開始終了時間";
  manualInput.appendChild(manualInputTitle);
  for (var i = 0; i < 6; i++) {
    // 時限ごと一行
    var row = document.createElement("div");
    row.setAttribute("class", "popup_setting_time_row");
    // 行のタイトル
    var title = document.createElement("h5");
    title.innerHTML = (i + 1) + "限";
    // 時限ごとの開始終了時間を入力する用
    var input1 = document.createElement("input");
    var input2 = document.createElement("input");
    input1.setAttribute("id", "popup_setting_start_time" + (i + 1));
    input2.setAttribute("id", "popup_setting_end_time" + (i + 1));
    input1.setAttribute("type", "time");
    input2.setAttribute("type", "time");
    // 波線
    var p = document.createElement("p");
    p.innerHTML = "~";
    // アペンド
    row.appendChild(title);
    row.appendChild(input1);
    row.appendChild(p);
    row.appendChild(input2);
    manualInput.appendChild(row);
  }
  // 保存ボタン
  var saveBtnWrapper = document.createElement("div");
  saveBtnWrapper.setAttribute("class", "popup_setting_time_save_button_wrapper");
  var saveBtn = document.createElement("button");
  saveBtn.setAttribute("id", "popup_setting_time_save_button");
  saveBtn.innerHTML = "保存";
  saveBtn.setAttribute("onclick", "settingTimeSave()");
  saveBtnWrapper.appendChild(saveBtn);
  manualInput.appendChild(saveBtnWrapper);
  wrapper.appendChild(manualInput);
}

// 時間の自動入力関数
function settingTimeAutoInput() {
  var inputValues = [];
  for (var i = 0; i < 4; i++) {
    if (i == 0){
      var tmp = document.getElementById("popup_setting_auto_input" + i).value;
      inputValues[i] = parseInt(tmp.split(":")[0])*60 + parseInt(tmp.split(":")[1]);
    }else {
      inputValues[i] = parseInt(document.getElementById("popup_setting_auto_input" + i).value);
    }
  }
  // 自動入力
  lastClassEndTime = inputValues[0] - inputValues[2];
  classTime = inputValues[1];
  for (var i = 1; i <= 6; i++) {
    var breaktime = inputValues[2];
    if (i == 3) { breaktime = inputValues[3]; }
    var startTime = lastClassEndTime + breaktime;
    var endTime = startTime + classTime;
    document.getElementById("popup_setting_start_time" + i).value = min2HHmm(startTime);
    document.getElementById("popup_setting_end_time" + i).value = min2HHmm(endTime);
    lastClassEndTime = lastClassEndTime + inputValues[1] + breaktime;
  }
}
// HH:mmに成形する関数
function min2HHmm(min) {
  var HH = Math.floor(min / 60);
  if (HH < 10) { HH = "0" + HH; }
  var mm = min % 60;
  if (mm < 10) { mm = "0" + mm; }
  return HH + ":" + mm;
}



// 時間を保存する関数
async function settingTimeSave() {
  var timeObj = {};
  timeObj.time = [];
  for (var i = 0; i < 6; i++) {
    var startTime = document.getElementById("popup_setting_start_time" + (i + 1)).value
    var endTime = document.getElementById("popup_setting_end_time" + (i + 1)).value;
    if (!startTime || !endTime) { alert("時間を入力してください。"); return; }
    timeObj.time[i] = [startTime, endTime];
  }
  await eel.set_start_end_time(timeObj);
  window.location.reload();
}

