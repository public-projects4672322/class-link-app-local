/////////////////////////////////////////////////////////////
// タイムテーブルのセットアップ
/////////////////////////////////////////////////////////////
// 設定から時限ごとの時刻を取得
async function getStartEndTime() {
  startEndTime = await eel.get_start_end_time()();
  return new Promise((resolve) => {
    resolve(startEndTime.time);
  })
}
// タイムスケジュールを取得
async function getTimescheduleFromJson() {
  var timeschedule = await eel.get_timeschedule()();
  return new Promise((resolve) => {
    resolve(timeschedule);
  });
}
// タイムテーブルを作成
async function makeTimetable(){
  var timeschedule = await getTimescheduleFromJson();
  makeTable(timeschedule);
}
// タイムテーブル作成関数
function makeTable(timeschedule) {
  var timescheduleByDay = [timeschedule.Mon, timeschedule.Tue, timeschedule.Wed, timeschedule.Thu, timeschedule.Fri, timeschedule.Sat];
  // タイムテーブル
  var timetable = document.getElementById("timetable");
  // 曜日見出し
  var tr = document.createElement("tr");
  th = document.createElement("th");
  th.setAttribute("class", "col_left");
  tr.appendChild(th);
  for (var i = 0; i < 6; i++){
    th = document.createElement("th");
    th.textContent = ['月', '火', '水', '木', '金', '土'][i];
    tr.appendChild(th);
  }
  timetable.appendChild(tr);

  // 時間
  for (var i = 1; i <= 6; i++){
    // 時間見出し
    tr = document.createElement("tr");
    td = document.createElement("td");
    td.innerHTML = i;
    small = document.createElement("small");
    small.innerHTML = startEndTime[i-1][0] + '<br>≀<br>' + startEndTime[i-1][1];
    td.appendChild(small);
    td.setAttribute("class", "col_left");
    tr.appendChild(td);
    // 授業
    for (var j = 1; j <= 6; j++){
      td = document.createElement("td");
      td.setAttribute("id", (j) + ',' + (i));
      td.setAttribute("class", "class");
      td.setAttribute("onclick", "popupClickedTd(this.id)");
      // クラス名表示用
      title = document.createElement("h2");
      title.setAttribute("class", "class_name");
      if (timescheduleByDay[j-1][i].class_name) {
        title.textContent = timescheduleByDay[j-1][i].class_name;
        td.classList.add("class_exists");
      }
      td.appendChild(title);
      tr.appendChild(td);
    }
    timetable.appendChild(tr);
  }
}


// 完成していない関数、採用するか不明
// 現在の授業をハイライト(タイムテーブル上の色を変える)
function highlightcurrentClass() {
  if (today[3] == 0) { return; } // 日曜なら何もしない
  if (!currentClassNum || currentClassNum < 1 || currentClassNum > 7) { return; }
  // 前の授業がハイライトされていたら元に戻す
  if (currentClassNum == 7){
    var element = document.getElementById( today[3] + ',6');
    removeClass(element, "highlight_current_class");
    return;
  }else if (currentClassNum > 1) {
    var element = document.getElementById( today[3] + ',' + (currentClassNum - 1));
    removeClass(element, "highlight_current_class");
  }
  // 現在の授業をハイライト
  var id = today[3] + ',' + currentClassNum
  id = '3,2';
  var td = document.getElementById(id);
  td.classList.add("highlight_current_class");
}


/////////////////////////////////////////////////////////////
// 指定された曜日時限の授業内容を取得
/////////////////////////////////////////////////////////////
function getClassInfoByDayAndPeriod(day, period) {
  var classInfo = {class_name: '', zoom_id: '', zoom_pass: '', classroom: '', teacher_name: ''};
  if (day < 1 || day > 6 && period > 0 && period < 7) { return; }
}



/////////////////////////////////////////////////////////////
// 現在または次の授業を出力
/////////////////////////////////////////////////////////////
function printClassInfoByCurrentTime(classInfo = false, currentOrNext, notime=false) {
  if (currentOrNext == 'c') {
    var className = document.getElementById('current_class_name');
    var classZoomID = document.getElementById('current_class_zoom_id');
    var classPassword = document.getElementById('current_class_password');
    var classLink1 = document.getElementById('current_class_link1_a');
    var classLink2 = document.getElementById('current_class_link2_a');
  } else if (currentOrNext == 'n') {
    var className = document.getElementById('next_class_name');
    var classZoomID = document.getElementById('next_class_zoom_id');
    var classPassword = document.getElementById('next_class_password');
    var classLink1 = document.getElementById('next_class_link1_a');
    var classLink2 = document.getElementById('next_class_link2_a');
  }
  
  // 授業名
  if (!classInfo.class_name) {
    if (notime) {
      className.textContent = '授業時間を設定してください';
      className.setAttribute('onclick', 'popupSettingWindow()');
      addClass(className, 'hover');
    }else {
      className.textContent = 'なし';
      removeClass(className, 'hover');
    }
  }else {
    className.textContent = classInfo.class_name;
    resizeClassName(className ,classInfo.class_name);
  }
  // ZoomID
  if (classInfo.zoom_id) {
    classZoomID.innerHTML = classInfo.zoom_id;
    if (currentOrNext == 'c') {
      var element = document.getElementById('copy_btn_c_i_1');
      removeClass(element, 'invisible');
    } else {
      var element = document.getElementById('copy_btn_n_i_3');
      removeClass(element, 'invisible');
    }
  } else {
    classZoomID.innerHTML = 'なし';
    if (currentOrNext == 'c') {
      var element = document.getElementById('copy_btn_c_i_1');
      addClass(element, 'invisible'); 
    }else {
      var element = document.getElementById('copy_btn_n_i_3');
      addClass(element, 'invisible'); 
    }
  }
  // パスワード
  if (classInfo.zoom_pass) {
    for (i in classInfo.zoom_pass) {
      classPassword.innerHTML += '*';
    }
    if (currentOrNext == 'c') {
      var element = document.getElementById('copy_btn_c_p_2');
      removeClass(element, 'invisible');
    } else {
      var element = document.getElementById('copy_btn_n_p_4');
      removeClass(element, 'invisible');
    }
  } else {
    classPassword.innerHTML = 'なし';
    if (currentOrNext == 'c') {
      var element = document.getElementById('copy_btn_c_p_2');
      addClass(element, 'invisible');
    } else {
      var element = document.getElementById('copy_btn_n_p_4');
      addClass(element, 'invisible');
    }
  }
  // リンク1
  if (classInfo == false) {
    // 授業の情報が存在しないとき
    classLink1.innerHTML = 'リンク1なし';
    addClass(classLink1, 'disable_a');
    classLink2.innerHTML = 'リンク2なし';
    addClass(classLink2, 'disable_a');
  } else {
    if (!classInfo.links.link1.link) {
      classLink1.innerHTML = 'リンク1なし';
      addClass(classLink1, 'disable_a');
    } else {
      classLink1.href = classInfo.links.link1.link;
      if (classInfo.links.link1.name) {
        classLink1.innerHTML = classInfo.links.link1.name;
      } else {
        classLink2.innerHTML = 'リンク1';
      }
      removeClass(classLink1, 'disable_a');
    }
    // リンク2
    if (!classInfo.links.link2.link) {
      classLink2.innerHTML = 'リンク2なし';
      addClass(classLink2, 'disable_a');
    } else {
      classLink2.href = classInfo.links.link2.link;
      if (classInfo.links.link2.name) {
        classLink2.innerHTML = classInfo.links.link2.name;
      } else {
        classLink2.innerHTML = 'リンク2';
      }
      removeClass(classLink2, 'disable_a');
    }
  }
  
}

// 授業名の文字サイズは幅に合わせて変更する
function resizeClassName(element, className) {
  if (className.length > 10) {
    var nameWidth = 220 / className.length;
    element.style.fontSize = nameWidth + 'px';
  } else {
    element.style.fontSize = '24px';
  }
}


/////////////////////////////////////////////////////////////
// コピーボタン
/////////////////////////////////////////////////////////////
function copyToClipboard(target, element){
  // コピーする
  var copyText;
  if (target == 'current_class_password') {
    copyText = currentClassPassword;
  } else if (target == 'next_class_password') {
    copyText = nextClassPassword;
  } else {
    copyText = document.getElementById(target).innerHTML;
  }
  navigator.clipboard.writeText(copyText);
  // コピー完了後の処理
  copyEnded(element);
}

function copyEnded(element) {
  var btn = document.getElementById(element);
  btn.classList.add('invisible');

  // グッド画像を表示するwrapper
  var i = element.slice(-1);
  var copy_btn_wrapper = document.getElementsByClassName('copy_btn_wrapper')[i-1];
  
  var hoverTxt = document.createElement('p');
  hoverTxt.textContent = 'Copied!';
  hoverTxt.setAttribute('class', 'hover_txt');
  console.log(copy_btn_wrapper);
  copy_btn_wrapper.appendChild(hoverTxt);

  setTimeout(function(){
    hoverTxt.remove();
    btn.classList.remove('invisible');
  }, 1500);
}