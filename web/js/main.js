// 以下をグローバル変数として定める
var today; // 今日の日付
var startEndTime; // 時限ごとの時刻
var timeschedule; // タイムスケジュール
var timescheduleByDay; // タイムスケジュールの各曜日
var currentClassNum; // 現在何コマ目の授業か
var currentClassPassword; // 現在の授業のパスワード
var nextClassPassword; // 次の授業のパスワード


/////////////////////////////////////////////////////////////
// 全体の処理
/////////////////////////////////////////////////////////////
// 読み込み時にタイムテーブルを作成
// グローバル変数も読み込む
window.onload = async function() {
  pr();
  today = getCurrentDate(); // 今日の日付
  startEndTime = await getStartEndTime(); // 時限ごとの時刻
  timeschedule = await getTimescheduleFromJson(); // タイムスケジュール
  timescheduleByDay = [timeschedule.Mon, timeschedule.Tue, timeschedule.Wed, timeschedule.Thu, timeschedule.Fri, timeschedule.Sat];
  makeTimetable(); // タイムテーブルを作成
  printTime(); // 現在時刻を表示
  printDate(); // 今日の日付を表示
  resizeWindow(); // ウィンドウサイズを調整
  currentClass(); // 現在の授業とその次の授業を表示
  // popupSettingWindow(); // 設定画面のポップアップ
}
async function pr() {
  var a = await eel.pr()
  console.log(a)
}

window.addEventListener('resize', resizeWindow);


/////////////////////////////////////////////////////////////
// 右側画面
/////////////////////////////////////////////////////////////
// 現在の授業とその次の授業を表示
function currentClass() {
  // 曜日を取得
  var dayOfWeek = today[3];
  dayOfWeek = 3
  if (dayOfWeek == 0) { return; }
  // 現在時刻を取得
  var hourMinSec = getCurrentTime();
  if (hourMinSec[0] == 0 && hourMinSec[1] == 0 && hourMinSec[2] == 0) {
    today = getCurrentDate(); 
  }
  // 時間を分に変換
  var currentTimeMin = hourMinSec[0] * 60 + hourMinSec[1];
  // currentTimeMin = 15 * 60 + 5;
  // 今の時刻と一致する授業を探す
  var classNum = 0; // 現在何コマ目の授業かを格納する
  // 休み時間のときは、次の授業とその2つ後の授業を表示する
  var breakFlg = false;
  for (var i = 0; i < startEndTime.length; i++) {
    startTime = parseInt(startEndTime[i][0].split(':')[0]) * 60 + parseInt(startEndTime[i][0].split(':')[1]);
    endTime = parseInt(startEndTime[i][1].split(':')[0]) * 60 + parseInt(startEndTime[i][1].split(':')[1]);
    if (!startTime || !endTime) { printClassInfoByCurrentTime(false, 'c', true); return; } // 時間が設定されていないとき
    if ( i < startEndTime.length - 1) { 
      nextStartTime = parseInt(startEndTime[i + 1][0].split(':')[0]) * 60 + parseInt(startEndTime[i + 1][0].split(':')[1]);
    }
    if (currentTimeMin >= startTime && currentTimeMin < endTime) {
      // 一致した授業の時限を取得
      classNum = i + 1;
      breakFlg = false;
      break;
    } else if (currentTimeMin >= endTime && currentTimeMin < nextStartTime && i < startEndTime.length - 1 ) {
      // 一致した授業の時限を取得
      classNum = i + 2;
      breakFlg = true;
      break;
    }
  }
  // 一致しなかった場合
  if (classNum == 0) {
    var currentClassName = document.getElementById("current_class_name").innerHTML;
    var nextClassName = document.getElementById("next_class_name").innerHTML;
    if (currentClassName == "") { printClassInfoByCurrentTime(false, 'c'); }
    if (nextClassName == "") { printClassInfoByCurrentTime(false, 'n'); }
    return;
  }
  // 一致した授業と現在の授業を比較して同じだったら表示内容を更新しない
  if (currentClassNum == classNum) { return; }
  currentClassNum = classNum; // 違ったら現在の授業を更新

  if (breakFlg) { // 休み時間だったら次の授業とその2つ後の授業を表示
    document.getElementById("current_class_title").innerHTML = "次の授業";
    document.getElementById("next_class_title").innerHTML = "2つ後の授業";
  } else {
    document.getElementById("current_class_title").innerHTML = "現在の授業";
    document.getElementById("next_class_title").innerHTML = "次の授業";
  }

  // 現在の授業を取得
  var classInfo = [timeschedule.Mon, timeschedule.Tue, timeschedule.Wed, timeschedule.Thu, timeschedule.Fri, timeschedule.Sat][dayOfWeek - 1][classNum];
  printClassInfoByCurrentTime(classInfo, 'c');
  currentClassPassword = classInfo.zoom_pass;

  // 次の授業も表示
  var nextClassNum = classNum + 1;
  if (nextClassNum > 6) { nextClassNum = 1; }
  var nextClassInfo = [timeschedule.Mon, timeschedule.Tue, timeschedule.Wed, timeschedule.Thu, timeschedule.Fri, timeschedule.Sat][dayOfWeek - 1][nextClassNum];
  printClassInfoByCurrentTime(nextClassInfo, 'n');
  nextClassPassword = nextClassInfo.zoom_pass;

  // 現在の授業をハイライト
  // highlightcurrentClass();
  
} setInterval(currentClass, 1000);



// 現在時刻を表示し続ける
function printTime(){
  var hourMinSec = getCurrentTime();
  var format = zeroPad(hourMinSec[0]) + ':' + zeroPad(hourMinSec[1]);
  document.getElementById('print_time').innerHTML = format;
}
setInterval(printTime, 1000);

function printDate(){
  var date = getCurrentDate();
  var format = zeroPad(date[0]) + '/' + zeroPad(date[1]) + '/' + zeroPad(date[2]) + '(' + ['日', '月', '火', '水', '木', '金', '土'][date[3]] + ')';
  document.getElementById('print_date').innerHTML = format;
}





/////////////////////////////////////////////////////////////
// 設定画面
/////////////////////////////////////////////////////////////
