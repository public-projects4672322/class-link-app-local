# Class Link App (local)

## アプリ概要

### アプリ実行画像
![実行例](UI_images/image-2.png)
![授業登録画面例](UI_images/image-4.png)
![授業登録後画面例](UI_images/image-3.png)
![設定画面](UI_images/image-5.png)

### アプリの特徴
通常の時間割に加えて2つまでurlを一緒に登録することができる。
そして現在時刻によって右側に授業が表示されるため、そこからZoomIDやPasswordをコピーしたり、登録したurlにワンクリックで遷移したりすることができる。


## 大まかな仕様
* pythonのeelライブラリを用いて、html,css,jsで作成したwebサイトをローカル環境で実行している。
* 授業の情報や授業開始時刻などの設定はjsonファイルにて保存している。
* 全体の流れとしては以下のとおりである。
    * pythonのeelでローカルアプリを起動する
    * pythonを用いてjsonファイルからデータを読み込む
    * そのデータをjsで処理し、html,cssで構築したwebページに適切に反映させる。


## 使用言語
* html
* css
* javascript
* python


## 作成年月日
2023/12


## 開発背景
大学の授業にてzoomに参加したりオンライン授業資料取得の手間を省こうと思い、作成。


## 課題

### 2024/05

* ローカル環境でしか動かないものであり、exeファイルをダウンロードしないと使えない。
* webでもアクセスできるようにしたり、dbを利用すればたくさんの人が使いやすいものになると思う。
* さらにdbを利用すれば、自分や他の人が登録したデータを共有・活用でき、利便性があがると思う。